package com.example.pokedex.utils

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val baseURL = "https://pokeapi.co/api/v2/"

fun <T> getRetrofitBuilder(endPont: Class<T>): T =
    Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build()
        .create(endPont)
