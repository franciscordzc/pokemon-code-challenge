package com.example.pokedex.domain.use_case

import com.example.pokedex.data.repository.PokedexAPI
import com.example.pokedex.data.response.pokemon_detail.PokemonDetailDTO
import com.example.pokedex.domain.model.PokemonDetail
import com.example.pokedex.utils.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetPokemonDetailUseCase @Inject constructor(
    private val pokedexAPI: PokedexAPI
) {
    operator fun invoke(pokemonId: Int): Flow<Resource<PokemonDetail>> = flow {
        try {
            emit(Resource.Loading())
            emit(Resource.Success(pokemonDetailDTOWrapper(pokedexAPI.getPokemonDetail(pokemonId))))
        } catch (e: HttpException) {
            emit(Resource.Error(e.fillInStackTrace()))
        } catch (e: IOException) {
            emit(Resource.Error(e.fillInStackTrace()))
        }
    }

    private fun pokemonDetailDTOWrapper(pokemonDetailDTO: PokemonDetailDTO): PokemonDetail =
        PokemonDetail(
            name = pokemonDetailDTO.name,
            height = pokemonDetailDTO.height,
            weight = pokemonDetailDTO.weight,
            order = pokemonDetailDTO.order
        )
}