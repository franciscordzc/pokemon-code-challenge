package com.example.pokedex.domain.model

data class PokemonDetail(
    val name: String = "",
    val height: Int = 0,
    val order: Int = 0,
    val weight: Int = 0
)