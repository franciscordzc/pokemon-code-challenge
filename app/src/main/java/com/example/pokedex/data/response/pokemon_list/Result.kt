package com.example.pokedex.data.response.pokemon_list

data class Result(
    val name: String,
    val url: String
)