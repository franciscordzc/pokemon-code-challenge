package com.example.pokedex.data.response.pokemon_detail

data class MoveLearnMethod(
    val name: String,
    val url: String
)