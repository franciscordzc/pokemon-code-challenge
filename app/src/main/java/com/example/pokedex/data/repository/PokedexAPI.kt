package com.example.pokedex.data.repository

import com.example.pokedex.data.response.pokemon_detail.PokemonDetailDTO
import com.example.pokedex.data.response.pokemon_list.PokemonListDTO
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokedexAPI {

    @GET("pokemon")
    suspend fun getPokemonList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): PokemonListDTO

    @GET("pokemon/{id}/")
    suspend fun getPokemonDetail(
        @Path("id") id: Int,
    ): PokemonDetailDTO

}