package com.example.pokedex.data.response.pokemon_detail

data class Version(
    val name: String,
    val url: String
)