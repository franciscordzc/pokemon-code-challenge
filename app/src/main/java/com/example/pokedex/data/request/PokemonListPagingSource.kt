package com.example.pokedex.data.request

import android.net.Uri
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.pokedex.data.repository.PokedexAPI
import com.example.pokedex.data.response.pokemon_list.Result
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class PokemonListPagingSource @Inject constructor(private val pokedexAPI: PokedexAPI) :
    PagingSource<Int, Result>() {

    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        return try {
            val nextPage = params.key ?: defaulIndex
            val response = pokedexAPI.getPokemonList(nextPage, defaultLimit)

            LoadResult.Page(
                data = response.results,
                prevKey = getKeysByParam(response.previous),
                nextKey = getKeysByParam(response.next)
            )

        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    private fun getKeysByParam(key: String?): Int? {
        return try {
            key?.let {
                val uri = Uri.parse(key)
                uri.getQueryParameter("offset")?.toInt() ?: 0
            }
        } catch (exception: IOException) {
            return null
        }
    }

    companion object {
        private const val defaulIndex = 0
        private const val defaultLimit = 20
    }

}