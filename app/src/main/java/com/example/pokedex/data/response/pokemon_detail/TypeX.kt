package com.example.pokedex.data.response.pokemon_detail

data class TypeX(
    val name: String,
    val url: String
)