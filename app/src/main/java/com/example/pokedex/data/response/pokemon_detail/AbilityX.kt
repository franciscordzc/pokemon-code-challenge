package com.example.pokedex.data.response.pokemon_detail

data class AbilityX(
    val name: String,
    val url: String
)