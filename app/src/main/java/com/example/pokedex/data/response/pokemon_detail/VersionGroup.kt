package com.example.pokedex.data.response.pokemon_detail

data class VersionGroup(
    val name: String,
    val url: String
)