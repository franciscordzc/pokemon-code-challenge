package com.example.pokedex.data.response.pokemon_detail

data class GenerationIi(
    val crystal: Crystal,
    val gold: Gold,
    val silver: Silver
)