package com.example.pokedex.data.response.pokemon_detail

data class StatX(
    val name: String,
    val url: String
)