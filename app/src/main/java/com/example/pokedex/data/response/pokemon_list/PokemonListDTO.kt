package com.example.pokedex.data.response.pokemon_list

data class PokemonListDTO(
    val count: Int,
    val next: String,
    val previous: String?,
    val results: List<Result>
)