package com.example.pokedex.data.response.pokemon_detail

data class Species(
    val name: String,
    val url: String
)