package com.example.pokedex.data.response.pokemon_detail

data class Type(
    val slot: Int,
    val type: TypeX
)