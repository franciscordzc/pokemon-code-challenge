package com.example.pokedex.data.response.pokemon_detail

data class MoveX(
    val name: String,
    val url: String
)