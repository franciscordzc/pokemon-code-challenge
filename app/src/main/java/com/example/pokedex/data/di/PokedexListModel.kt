package com.example.pokedex.data.di

import com.example.pokedex.data.repository.PokedexAPI
import com.example.pokedex.utils.getRetrofitBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object PokedexListModel {

    @Provides
    @Singleton
    fun providesPokedexAPI(): PokedexAPI = getRetrofitBuilder(
        PokedexAPI::class.java
    )

}