package com.example.pokedex.data.response.pokemon_detail

data class Form(
    val name: String,
    val url: String
)