package com.example.pokedex.ui.pokemon_list

fun interface PokemonListEvents {
    fun onPokemonSelected(id: Int)
}