package com.example.pokedex.ui.pokemon_list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pokedex.R
import com.example.pokedex.databinding.FragmentPokemonListBinding
import com.example.pokedex.domain.model.PokemonDetail
import com.example.pokedex.ui.PokedexViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
@WithFragmentBindings
class PokemonListFragment : Fragment(), PokemonListEvents {

    private lateinit var pokemonList: PokemonListAdapter
    private val pokemonListBinding get() = _pokemonListBinding!!
    private var _pokemonListBinding: FragmentPokemonListBinding? = null
    private val pokedexViewModel: PokedexViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initViewBinding(inflater, container)

    override fun onDestroyView() {
        super.onDestroyView()
        _pokemonListBinding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPokemonListAdapter()
        getPokemonList()
    }

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _pokemonListBinding = FragmentPokemonListBinding.inflate(inflater, container, false)
        return pokemonListBinding.root
    }

    private fun initPokemonListAdapter() {
        pokemonListBinding.pokemonListRecycler.apply {
            pokemonList = PokemonListAdapter(this@PokemonListFragment)
            adapter = pokemonList
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun getPokemonList() {
        lifecycleScope.launch {
            pokedexViewModel.getPokemonList().collectLatest {
                Log.d("POKEMON: ", it.toString())
                pokemonList.submitData(it)
            }
        }
    }

    private fun getPokemonDetail(pokemonDetail: PokemonDetail) {
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(pokemonDetail.name)
                .setMessage(
                    resources.getString(
                        R.string.pokemon_detail,
                        pokemonDetail.height.toString(),
                        pokemonDetail.weight.toString(),
                        pokemonDetail.order.toString()
                    )
                )
                .setPositiveButton(resources.getString(R.string.accept)) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }
    }

    override fun onPokemonSelected(id: Int) {
        pokedexViewModel.getPokemonDetail(id)
        pokedexViewModel.pokemonDetail.observe(viewLifecycleOwner) {
            getPokemonDetail(it)
        }
    }

}