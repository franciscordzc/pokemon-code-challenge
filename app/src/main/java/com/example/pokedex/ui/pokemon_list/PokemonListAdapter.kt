package com.example.pokedex.ui.pokemon_list

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.pokedex.data.response.pokemon_list.Result
import com.example.pokedex.databinding.LayoutPokemonNameBinding

class PokemonListAdapter(private val pokemonListEvents: PokemonListEvents) :
    PagingDataAdapter<Result, PokemonListAdapter.PokemonListViewHolder>(PokemonListDiffCallback()) {

    override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
        getItem(position)?.let {
            holder.setPokemonName(it)
            holder.setOnSelectedPokemon(it, position + 1)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder =
        PokemonListViewHolder(LayoutPokemonNameBinding.inflate(LayoutInflater.from(parent.context)))

    inner class PokemonListViewHolder(private val binding: LayoutPokemonNameBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun setPokemonName(result: Result) {
            binding.pokemonText.text = result.name
        }

        fun setOnSelectedPokemon(result: Result, id: Int) {
            binding.pokemonCard.setOnClickListener {
                Log.d("POKEMON: ", result.name)
                pokemonListEvents.onPokemonSelected(id)
            }
        }
    }

    class PokemonListDiffCallback : DiffUtil.ItemCallback<Result>() {
        override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean =
            oldItem.name == newItem.name

        override fun areContentsTheSame(oldItem: Result, newItem: Result): Boolean =
            oldItem.name == newItem.name
                    && oldItem.url == newItem.url

    }

}