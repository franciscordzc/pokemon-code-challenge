package com.example.pokedex.ui

import SingleLiveEvent
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.pokedex.data.request.PokemonListPagingSource
import com.example.pokedex.data.response.pokemon_list.Result
import com.example.pokedex.domain.model.PokemonDetail
import com.example.pokedex.domain.use_case.GetPokemonDetailUseCase
import com.example.pokedex.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class PokedexViewModel @Inject constructor(
    private val pokemonListPagingSource: PokemonListPagingSource,
    private val getPokemonDetailUseCase: GetPokemonDetailUseCase
) : ViewModel() {

    private val _getPokemonDetail = SingleLiveEvent<PokemonDetail>()
    val pokemonDetail: LiveData<PokemonDetail> get() = _getPokemonDetail

    fun getPokemonList(): Flow<PagingData<Result>> =
        Pager(config = PagingConfig(pageSize = 20),
            pagingSourceFactory = {
                pokemonListPagingSource
            }).flow.cachedIn(viewModelScope)

    fun getPokemonDetail(id: Int) {
        getPokemonDetailUseCase(id).onEach { result ->
            when (result) {
                is Resource.Loading -> println(result.message)
                is Resource.Error -> println(result.message)
                is Resource.Success -> _getPokemonDetail.value = result.data ?: PokemonDetail()
            }
        }.launchIn(viewModelScope)
    }


}