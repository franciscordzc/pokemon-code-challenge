@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.kotlinAndroid)
    alias(libs.plugins.androidHilt)
    kotlin("kapt")
}

android {
    namespace = "com.example.pokedex"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.example.pokedex"
        minSdk = 27
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions.jvmTarget = "17"
    kapt.useBuildCache = false
    buildFeatures.viewBinding = true
}

dependencies {
    implementation(libs.core.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.coil)
    implementation(libs.androidx.multidex)
    implementation(libs.androix.paging)
    implementation(libs.androidx.lifecycle)
    implementation(libs.retrifit.gson)
    implementation(libs.coroutines)
    implementation(libs.android.hilt)
    implementation(libs.navigation)
    implementation(libs.navigation.ui)
    kapt(libs.android.hilt.compiler)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
}